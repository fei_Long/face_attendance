# 模块划分

1. 视频采集 -- V4L，OpenCV
2. 人脸检测 -- 离线检测，识别照片中的哪一部分是人脸
3. 人脸识别 -- 在线识别，对比采集的照片中和事先录入的照片是否是同一个人
4. 入库 -- 记录到考勤机本地的数据库中
5. 信息显示 -- 直接显示到屏幕上

硬件：摄像头（分辨率），ARM Cortex A系列开发板，本地存储（Flash容量），TFT液晶屏幕（分辨率，大小），内存容量，网络连接方式（可选，有线/无线，带宽）

raspberry pi 树莓派

软件：Linux系统（Raspbian/debian10），OpenCV（3.2版本），云服务



## 什么是OpenCV

OpenCV Intel公司发布的一款计算机视觉库，用于图像分析。

## 用OpenCV做什么

调用摄像头驱动采集图像，对图像进行预处理，用于人脸检测的物体检测算法（基于决策树的机器学习算法）

## 安装Raspbian

1. （可选）修改软件源 https://mirrors.tuna.tsinghua.edu.cn/help/debian/
2. 安装vim

```bash
sudo apt install vim
```

3. 安装opencv开发软件包

```bash
sudo apt update
sudo apt install libopencv-dev
```

## 打开SSH服务

```bash
sudo raspi-config
```



## OpenCV读取摄像头

```c++
#include <opencv2/opencv.hpp>

using namespace cv;

int main()
{
    VideoCapture cam(0); //创建摄像头对象
    namedWindow("Camera"); //创建名字为Camera的窗口
    
    Mat image;
    
    while(1)
    {
        cam >> image; //从摄像头读取一帧图像
    	imshow("Camera", image); //在Camera窗口显示摄像头捕获到的图像
    	if (waitKey(10) != 255) //延时10毫秒，判断是否有按键按下，如果按下结束程序
        {
            break;
        }
    }
}
```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui 
```



## OpenCV人脸检测

```C++
#include <opencv2/opencv.hpp>
#include <vector>

using namespace cv;
using namespace std;

int main()
{
	VideoCapture cam(0);
    //创建级联分类器
	CascadeClassifier classifier("/usr/share/opencv/haarcascades/haarcascade_frontalface_alt2.xml");

	namedWindow("Camera");

	Mat image;
	Mat gray; //保存转换后的灰度图像

	while(1)
	{
		cam >> image;
		cvtColor(image, gray, COLOR_BGR2GRAY);  //将3通道的彩色图像转换为单通道灰度图像
		equalizeHist(gray, gray);  //对图像进行均衡化处理，提高图像对比度，优化分类器的效果
		vector<Rect> faces;  //定义数组，保存检测到的人脸区域
		classifier.detectMultiScale(gray, faces); //检测人脸
		if (faces.size())  //是否找到人脸
		{
			rectangle(image, faces[0], CV_RGB(255, 0, 0)); //将找到的人脸区域用矩形框标记出来
		}
		imshow("Camera", image);
		if (waitKey(40) != 255)
		{
			break;
		}
	}
}

```

编译命令：

```bash
g++ cam.cpp -lopencv_core -lopencv_videoio -lopencv_highgui -lopencv_imgproc -lopencv_objdetect
```

